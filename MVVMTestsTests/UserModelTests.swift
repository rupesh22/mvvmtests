//
//  UserModelTests.swift
//  MVVMTestsTests
//
//  Created by Mac HD on 04/03/22.
//

import XCTest
@testable import MVVMTests
class UserModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testUserFullnameExists() {
        let user = User(id: 1, email: "abc@gmail.com", avatar: "", firstName: "John", lastName: "Snow")
        let sut = UserDetailsViewModel(user: UserViewModel(user: user))
        
        XCTAssertEqual("John Snow", sut.user.name)
    }

    func testUserHasValidEmail() {
        let user = User(id: 1, email: "abc@gmail.com", avatar: "", firstName: "John", lastName: "Snow")
        let sut = UserDetailsViewModel(user: UserViewModel(user: user))
        
        XCTAssertEqual("abc@gmail.com", sut.user.email)
    }
    
    
}
