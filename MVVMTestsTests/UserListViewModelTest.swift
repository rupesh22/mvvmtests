//
//  UserListViewModelTest.swift
//  MVVMTestsTests
//
//  Created by Mac HD on 24/02/22.
//

import XCTest
@testable import MVVMTests

class UserListViewModelTest: XCTestCase {

    let json = """
{
  "page": 1,
  "per_page": 6,
  "total": 12,
  "total_pages": 2,
  "data": [
    {
      "id": 1,
      "email": "george.bluth@reqres.in",
      "first_name": "George",
      "last_name": "Bluth",
      "avatar": "https://reqres.in/img/faces/1-image.jpg"
    },
    {
      "id": 2,
      "email": "janet.weaver@reqres.in",
      "first_name": "Janet",
      "last_name": "Weaver",
      "avatar": "https://reqres.in/img/faces/2-image.jpg"
    },
    {
      "id": 3,
      "email": "emma.wong@reqres.in",
      "first_name": "Emma",
      "last_name": "Wong",
      "avatar": "https://reqres.in/img/faces/3-image.jpg"
    },
    {
      "id": 4,
      "email": "eve.holt@reqres.in",
      "first_name": "Eve",
      "last_name": "Holt",
      "avatar": "https://reqres.in/img/faces/4-image.jpg"
    },
    {
      "id": 5,
      "email": "charles.morris@reqres.in",
      "first_name": "Charles",
      "last_name": "Morris",
      "avatar": "https://reqres.in/img/faces/5-image.jpg"
    },
    {
      "id": 6,
      "email": "tracey.ramos@reqres.in",
      "first_name": "Tracey",
      "last_name": "Ramos",
      "avatar": "https://reqres.in/img/faces/6-image.jpg"
    }
  ],
  "support": {
    "url": "https://reqres.in/#support-heading",
    "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
  }
}
"""
    
    var sut : UserListViewModel!
    
    func setupData() {
        
        let userRepository = UserListrepository(with: NetworkManager.shared)
        sut = UserListViewModel(userListRepository: userRepository)
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCanParseUserData() throws {
        let jsonData = json.data(using: .utf8)!
        let userData = try! JSONDecoder().decode(UsersResponse.self, from: jsonData)
        
        XCTAssertEqual("https://reqres.in/#support-heading", userData.support.url)
        XCTAssertEqual("To keep ReqRes free, contributions towards server costs are appreciated!", userData.support.text)
    }
    
    func testCanParseUserDataWithNoUser() throws {
        
        let json2 = """
    {
      "page": 1,
      "per_page": 6,
      "total": 12,
      "total_pages": 2,
      "data": [
      ],
      "support": {
        "url": "https://reqres.in/#support-heading",
        "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
      }
    }
    """
        
        let jsonData = json2.data(using: .utf8)!
        let userData = try! JSONDecoder().decode(UsersResponse.self, from: jsonData)
        
        XCTAssertTrue(userData.data.isEmpty)
    }
    
    func testCanParseUserDataWithJSONFile() {
        
       guard let path = Bundle(for: type(of: self)).path(forResource: "userData", ofType: "json") else {
            fatalError("json file not found")
        }
        guard let jsonFile = try? String(contentsOfFile: path, encoding: .utf8) else {
            fatalError("unable to convert string to json")
        }
        
        let jsonData = jsonFile.data(using: .utf8)!
        let userData = try! JSONDecoder().decode(UsersResponse.self, from: jsonData)
        
        XCTAssertFalse(userData.data.isEmpty)
        
    }
    
    func testCanCheckUserNameWithJSONFile() {
        
       guard let path = Bundle(for: type(of: self)).path(forResource: "userData", ofType: "json") else {
            fatalError("json file not found")
        }
        guard let jsonFile = try? String(contentsOfFile: path, encoding: .utf8) else {
            fatalError("unable to convert string to json")
        }
        
        let jsonData = jsonFile.data(using: .utf8)!
        let userData = try! JSONDecoder().decode(UsersResponse.self, from: jsonData)
        
        XCTAssertEqual("George", userData.data.first?.firstName)
        
    }
    
    func testFetchUserDataCount() {
        
        //Given
        setupData()
        let expected = XCTestExpectation(description: "UserListViewModel fetch User Data")
        
        //When
        sut.getUsers()

        //THEN
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let users = self.sut.users
            XCTAssertNotNil(users)
            XCTAssertEqual(users.count, 6, "Count must be 6")
            expected.fulfill() // Expectation process is complete
        }
        
        wait(for: [expected], timeout: 5) //wait for 5 seconds or throw error
    }

    func testCheckFirstUserName() {
        //Given
        setupData()
        let expected = XCTestExpectation(description: "UserListViewModel fetch User Data")
        
        //When
        sut.getUsers()

        //THEN
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let users = self.sut.users
            XCTAssertNotNil(users)
            XCTAssertEqual(users.first?.name, "George Bluth", "username does not match")
            expected.fulfill() // Expectation process is complete
        }
        
        wait(for: [expected], timeout: 5) //wait for 5 seconds or throw error
    }
    
    func testViewModelFailedToFetchData() {
        //Given
        setupData()
        let expected = XCTestExpectation(description: "UserListViewModel fetch User Data")
        
        //When
        sut.getUsers()

        //THEN
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let users = self.sut.users
            XCTAssertNotNil(users)
            XCTAssertEqual(users.first?.name, "George Bluth", "username does not match")
            expected.fulfill() // Expectation process is complete
        }
        
        wait(for: [expected], timeout: 5) //wait for 5 seconds or throw error
        
    }
}
