//
//  UserDetailsViewController.swift
//  MVVMTests
//
//  Created by Mac HD on 03/03/22.
//

import UIKit

final class UserDetailsViewController: UIViewController {
    
    //MARK: VIEWS
    private lazy var userImageView : UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.accessibilityIdentifier = AppaccessibilityIdentifier.userImage
        return imageView
    }()
    
    private lazy var userNameLabel : UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20.0, weight: .medium)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.accessibilityIdentifier = AppaccessibilityIdentifier.userName
        return label
    }()
    
    private lazy var userEmailLabel : UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14.0, weight: .regular)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.accessibilityIdentifier = AppaccessibilityIdentifier.userEmail

        return label
    }()
    
    //MARK: PROPERTIES
    private let viewModel: UserDetailsViewModel
    
    //MARK: LIFECYCLE METHODS
    
    init(viewModel: UserDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        prepareUI()
    }
    
}


//MARK: - SETUP
extension UserDetailsViewController {
    
    private func prepareUI() {
        view.accessibilityIdentifier = "userDetails"
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(userImageView)
        self.view.addSubview(userNameLabel)
        self.view.addSubview(userEmailLabel)
        NSLayoutConstraint.activate([
            
            //PRODUCT IMAGEVIEW CONSTRAINT
            userImageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: AutoLayoutConstraints.top),
            userImageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: AutoLayoutConstraints.left),
            userImageView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -AutoLayoutConstraints.right),
            userImageView.heightAnchor.constraint(equalToConstant: AutoLayoutConstraints.imageHeight),
            
            //PRODUCT TITLE CONSTRAINT
            userNameLabel.topAnchor.constraint(equalTo: userImageView.safeAreaLayoutGuide.bottomAnchor, constant: AutoLayoutConstraints.top),
            userNameLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: AutoLayoutConstraints.left),
            userNameLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -AutoLayoutConstraints.right),
            
            //PRODUCT DESCRIPTION CONSTRAINT
            userEmailLabel.topAnchor.constraint(equalTo: userNameLabel.safeAreaLayoutGuide.bottomAnchor, constant: AutoLayoutConstraints.top),
            userEmailLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: AutoLayoutConstraints.left),
            userEmailLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -AutoLayoutConstraints.right)
            
        ])
    }
    
    private func setupData() {
        self.title = viewModel.user.name
        let imageUrl = URL(string: viewModel.user.image)
        self.userImageView.setImage(url: imageUrl!)
        self.userNameLabel.text = viewModel.user.name
        self.userEmailLabel.text = viewModel.user.email
    }
}
