//
//  UserDetailsViewModel.swift
//  MVVMTests
//
//  Created by Mac HD on 03/03/22.
//

import Foundation

class UserDetailsViewModel {
    
    //MARK: PROPERTIES
    var user: UserViewModel
    
    init(user: UserViewModel) {
        self.user = user
    }
}
