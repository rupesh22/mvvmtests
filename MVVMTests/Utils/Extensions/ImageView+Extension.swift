//
//  ImageView+Extension.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import UIKit
import SDWebImage

extension UIImageView {
    func setImage(url : URL) {
        self.sd_setImage(with: url, completed: nil)
    }
}

