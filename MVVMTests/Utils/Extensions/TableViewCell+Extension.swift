//
//  TableViewCell+Extension.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import UIKit

extension UITableViewCell {
  static var reuseIdentifier: String {
    return NSStringFromClass(self)
  }
}
