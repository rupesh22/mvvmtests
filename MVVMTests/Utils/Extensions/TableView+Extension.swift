//
//  TableView+Extension.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import UIKit

extension UITableView {
  public func register<T: UITableViewCell>(_ cellClass: T.Type) {
    register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
  }

  public func dequeue<T: UITableViewCell>(_ cellClass: T.Type, for indexPath: IndexPath) -> T {
    guard let cell = dequeueReusableCell(
      withIdentifier: cellClass.reuseIdentifier, for: indexPath) as? T else {
        fatalError(
          "Error: cell with id: \(cellClass.reuseIdentifier) for indexPath: \(indexPath) is not \(T.self)")
    }
    return cell
  }
}
