//
//  Constraints.swift
//  MVVMTests
//
//  Created by Mac HD on 04/03/22.
//

import Foundation
import UIKit

//MARK: - AUTOLAYOUT CONSTRAINTS
struct AutoLayoutConstraints {
    
    static let top : CGFloat = 20.0
    static let left : CGFloat = 20.0
    static let right : CGFloat = 20.0
    static let bottom : CGFloat = 20.0
    static let cellImageWidth : CGFloat = 150.0
    static let cellImageHeight : CGFloat = 50.0
    static let imageHeight : CGFloat = 200
    static let titleHeight : CGFloat = 50.0
    
}
