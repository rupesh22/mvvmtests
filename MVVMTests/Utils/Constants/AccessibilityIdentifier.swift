//
//  AccessibilityIdentifiers.swift
//  MVVMTests
//
//  Created by Mac HD on 04/03/22.
//

import Foundation

struct AppaccessibilityIdentifier {
    static let userName = "userName"
    static let userImage = "userName"
    static let userEmail = "userEmail"
}
