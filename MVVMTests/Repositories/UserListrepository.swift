//
//  UserListrepository.swift
//  MVVMTests
//
//  Created by Mac HD on 02/03/22.
//

import Foundation

class UserListrepository {
   
    private let http: NetworkManager
    
    init(with networkManager: NetworkManager) {
        self.http = networkManager
    }
    
    func getUsers(completion: @escaping (Result<[User],APIError>) -> Void ) {
        let url = "https://reqres.in/api/users"
        
        do {
            try http.GET(type: UsersResponse.self, urlString: url) { result in
                switch result {
                case .success(let response):
                   print(response)
                    completion(.success(response.data))
                case .failure(let error):
                        print("Failed to make API call \(#function) in \(#file)")
                    completion(.failure(error))
                }
            }
        } catch {
            print("Failed to make API call \(#function) in \(#file)")
        }
    }
    
}
