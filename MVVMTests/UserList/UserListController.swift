//
//  ViewController.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import UIKit

final class UserListController: UIViewController {
    
    //MARK: PROPERTIES
    private let userListViewModel: UserListViewModel
    
    //MARK: DELEGATES
    weak var mainCoordinator: MainCoordinator?
    
    //MARK: INIT
    init(userListViewModel: UserListViewModel) {
        self.userListViewModel = userListViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: VIEWS
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UserListTableViewCell.self)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    //MARK: LIFECYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        userListViewModel.delegate = self
        userListViewModel.getUsers()
    }
    
}

//MARK: TABLEVIEW METHODS
extension UserListController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListViewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(UserListTableViewCell.self, for: indexPath)
        let user = userListViewModel.users[indexPath.row]
        cell.configure(with: user)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = userListViewModel.users[indexPath.row]
        self.mainCoordinator?.navigateToUser(with: user)
    }
}

//MARK: VIEWMODEL DELEGATE METHODS
extension UserListController: UserListViewModelDelegate {
    func onSuccesFullUserFound() {
        self.tableView.reloadData()
    }
    
    func onError(error: APIError) {
        print("Error: /\(error.localizedDescription)")
    }
}

//MARK: SETUP
private extension UserListController {
    func setupUI() {
        title = "Users"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

