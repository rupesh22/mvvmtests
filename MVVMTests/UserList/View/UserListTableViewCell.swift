//
//  UserListTableViewCell.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import UIKit

final class UserListTableViewCell: UITableViewCell {
    
    //MARK: - VIEWS
    private lazy var userImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .gray.withAlphaComponent(0.65)
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    private lazy var emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .systemGray
        return label
    }()
    
    //MARK: - INIT METHODS
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addSubview(userImageView)
        addSubview(userNameLabel)
        addSubview(emailLabel)
        
        NSLayoutConstraint.activate([
            userImageView.heightAnchor.constraint(equalToConstant: 75),
            userImageView.widthAnchor.constraint(equalToConstant: 75),
            userImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            userImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            
            userNameLabel.topAnchor.constraint(equalTo: userImageView.topAnchor),
            userNameLabel.leadingAnchor.constraint(equalTo: userImageView.trailingAnchor, constant: 8),
            userNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
            emailLabel.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor),
            emailLabel.leadingAnchor.constraint(equalTo: userImageView.trailingAnchor, constant: 8),
            emailLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
    }
    
    //MARK: - FUNCTIONS
    func configure(with user: UserViewModel) {
        userNameLabel.text = user.name
        emailLabel.text = user.email
        guard let url = URL(string: user.image) else { return }
        userImageView.setImage(url: url)
    }
}
