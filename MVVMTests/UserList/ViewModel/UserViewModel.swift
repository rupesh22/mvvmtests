//
//  UserViewModel.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import Foundation
import UIKit

class UserViewModel {
    
    var name: String {
        return "\(user.firstName) \(user.lastName)"
    }
    var email: String {
        return user.email
    }
    var image: String {
        return user.avatar
    }
    
    private var user: User
    
    init(user: User) {
        self.user = user
    }
}
