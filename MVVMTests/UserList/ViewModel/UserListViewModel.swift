//
//  UserViewModel.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import Foundation

protocol UserListViewModelDelegate: AnyObject {
    func onSuccesFullUserFound()
    func onError(error: APIError)
}

class UserListViewModel {
    
    //MARK: PROPERTIES
    
    private(set) var users = [UserViewModel]()
    public weak var delegate: UserListViewModelDelegate?
    
    private let repo: UserListrepository
    
    //MARK: INIT METHODS
    init(userListRepository: UserListrepository) {
        self.repo = userListRepository
    }
    
    
    //MARK: FUNCTIONS
    func getUsers() {
        
        repo.getUsers { result in
            switch result {
            case .success(let users):
                DispatchQueue.main.async {
                    self.users = users.map({ UserViewModel(user: $0) })
                    print(users)
                    self.delegate?.onSuccesFullUserFound()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    print("Failed to make API call \(#function) in \(#file)")
                    self.delegate?.onError(error: error)
                }
            }
        }
    }
}
    

