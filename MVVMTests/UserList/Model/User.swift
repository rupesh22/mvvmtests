//
//  User.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import Foundation

struct User: Codable {
    let id: Int
    let email: String
    let avatar: String
    let firstName: String
    let lastName: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case email
        case avatar
        case firstName = "first_name"
        case lastName = "last_name"
    }
}
