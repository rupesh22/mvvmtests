//
//  UsersResponse.swift
//  MVVMTests
//
//  Created by Mac HD on 24/02/22.
//

import Foundation

//MARK: USERRESPONSE
struct UsersResponse: Codable {
    let page : Int
    let perPage: Int
    let total: Int
    let totalPages: Int
    let data: [User]
    let support: Support
    
        enum CodingKeys: String, CodingKey {
            case page
            case perPage = "per_page"
            case total
            case totalPages = "total_pages"
            case data
            case support
        }
}

// MARK: - Support
struct Support: Codable {
    let url: String
    let text: String
}
