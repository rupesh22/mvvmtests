//
//  MainCoordinator.swift
//  MVVMTests
//
//  Created by Mac HD on 03/03/22.
//

import UIKit

protocol AppFlowCoordinator : AnyObject {
        var navigationController: UINavigationController { get set }
        func start()
}

class MainCoordinator : AppFlowCoordinator {

    var navigationController: UINavigationController
    init(with navigationController : UINavigationController){
        self.navigationController = navigationController
    }

    func start() {
        let userRepository = UserListrepository(with: NetworkManager.shared)
        let userViewModel = UserListViewModel(userListRepository: userRepository)
        let vc = UserListController(userListViewModel: userViewModel)
        vc.mainCoordinator = self
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    func navigateToUser(with details: UserViewModel) {
        let viewModel = UserDetailsViewModel(user: details)
        let vc = UserDetailsViewController(viewModel: viewModel)
        self.navigationController.pushViewController(vc, animated: true)
    }
}
