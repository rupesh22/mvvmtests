//
//  MVVMTestsUITests.swift
//  MVVMTestsUITests
//
//  Created by Mac HD on 24/02/22.
//

import XCTest

class MVVMTestsUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
    
    func testUserListScreenExist() {
            let app = XCUIApplication()
            app.launch()
            let tables = app.tables
            let selectedCellTitle = "George Bluth"
            let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
            tableCell.element.tap() // Simulate to 1st cell tap
                     
            //Check Screen if User Name Exits
            let username = app.staticTexts[selectedCellTitle]
            XCTAssertTrue(username.exists)
        }
    
    func testUserNameWithUserEmail() {
        
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap()
                 
        // Check Email Exist User Name Exits
        let username = app.staticTexts["george.bluth@reqres.in"]
        XCTAssertTrue(username.exists)
        
    }
    
    func testUserDetailsScreenExists(){
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap() // Simulate to 1st cell tap and Navigate to Details Screen
                 
        //Check Screen if Exist
        XCTAssertTrue(app.otherElements["userDetails"].waitForExistence(timeout: 5))
        //Check on details Screen
        let detailsTitle = app.staticTexts[selectedCellTitle]
        XCTAssertTrue(detailsTitle.exists)
    }
    
    func testUserDetailsIsValid(){
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap()
        
        let userdetailsElement = app.otherElements["userDetails"]
        userdetailsElement.tap()
        userdetailsElement.staticTexts["George Bluth"].tap()
        let email = app.staticTexts["george.bluth@reqres.in"]
        app.staticTexts["george.bluth@reqres.in"].tap()
        XCTAssertTrue(email.exists)

    }
    
    func testUserImageExists() {
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap()
        let image = app.images["userImage"]
        XCTAssertTrue(image.exists)
    }
    
    func testUserNameExists() {
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap()
        let name = app.staticTexts["userName"]
        XCTAssertTrue(name.exists)
    }
    
    func testUserEmailExists() {
        let app = XCUIApplication()
        app.launch()
        let tables = app.tables
        let selectedCellTitle = "George Bluth"
        let tableCell = tables.cells.containing(.staticText, identifier: selectedCellTitle)
        tableCell.element.tap()
        let email = app.staticTexts["userEmail"]
        XCTAssertTrue(email.exists)
    }
}
